/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import Utilities.ToolsUtilities;
import com.codename1.components.InfiniteProgress;
import com.codename1.demos.signin.UserForm;
import com.codename1.facebook.FaceBookAccess;
import com.codename1.io.AccessToken;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Oauth2;
import com.codename1.io.Storage;
import com.codename1.social.FacebookConnect;
import com.codename1.social.GoogleConnect;
import com.codename1.social.Login;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.util.StringUtil;
import com.mycompany.Entite.User;
import com.mycompany.myapp.MyApplication;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Map;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author cobwi
 */
public class LoginForm extends Form {

    private Form main;
    public static String TOKEN;

    private Form loginForm;

    private Resources theme;
    private Login login;
    final TextField username = new TextField();
    final TextField password = new TextField();

    public LoginForm(Form f) {
        super("Login Form");
        try {
            theme = Resources.openLayered("/theme");
            UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.main = f;

        this.setLayout(new BorderLayout());
        Container center = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        center.setUIID("ContainerWithPadding");

        Image logo = theme.getImage("CodenameOne.png");
        Label l = new Label(logo);
        Container flow = new Container(new FlowLayout(Component.CENTER));
        flow.addComponent(l);
        center.addComponent(flow);

        username.setHint("Username");
        password.setHint("Password");
        password.setConstraint(TextField.PASSWORD);

        center.addComponent(username);
        center.addComponent(password);

        Button signIn = new Button("Sign In");

        signIn.addActionListener(x -> {
            siginIn(main);
        });

        center.addComponent(signIn);
        this.addComponent(BorderLayout.CENTER, center);

        Container bottom = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Button loginWFace = new Button(theme.getImage("SignInFacebook.png_veryHigh.png"));
        //Button loginWFace = new Button("Facebook");
        loginWFace.setUIID("LoginButton");
        loginWFace.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {

                //create your own app identity on facebook follow the guide here:
                //facebook-login.html
                String clientId = "160701677931552";
                String redirectURI = "https://www.codenameone.com/";
                String clientSecret = "dfe9f991aaf7267fa0c70a9f79295004";
                //String clientSecret = "2532a457d1ca5c67196df97f9089799d";

                if (clientSecret.length() == 0) {
                    System.err.println("create your own facebook app follow the guide here:");
                    System.err.println("http://www.codenameone.com/facebook-login.html");
                    return;
                }
                Login fb = FacebookConnect.getInstance();
                fb.setClientId(clientId);
                fb.setRedirectURI(redirectURI);
                fb.setClientSecret(clientSecret);
                login = fb;
                fb.setCallback(new LoginListener(LoginListener.FACEBOOK));
                if (!fb.isUserLoggedIn()) {
                    fb.doLogin();
                } else {
                    showFacebookUser(fb.getAccessToken().getToken());
                }
            }
        });
        Button loginWG = new Button(theme.getImage("signin_google.png"));
        loginWG.setUIID("LoginButton");
        loginWG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                //create your own google project follow the guide here:
                //http://www.codenameone.com/google-login.html
                String clientId = "839004709667-n9el6dup3gono67vhi5nd0dm89vplrka.apps.googleusercontent.com";
                String redirectURI = "https://www.codenameone.com/oauth2callback";
                String clientSecret = "";

                if (clientSecret.length() == 0) {
                    System.err.println("create your own google project follow the guide here:");
                    System.err.println("http://www.codenameone.com/google-login.html");
                    return;
                }

                Login gc = GoogleConnect.getInstance();
                gc.setClientId(clientId);
                gc.setRedirectURI(redirectURI);
                gc.setClientSecret(clientSecret);
                login = gc;
                gc.setCallback(new LoginListener(LoginListener.GOOGLE));
                if (!gc.isUserLoggedIn()) {
                    gc.doLogin();
                } else {
                    showGoogleUser(gc.getAccessToken().getToken());
                }
            }
        });

        bottom.addComponent(loginWFace);
        bottom.addComponent(loginWG);

        this.addComponent(BorderLayout.SOUTH, bottom);
        //loginForm.show();
    }

    private void siginIn(final Form main) {

        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost:7070/piwebService/webresources/com.mycompany.piwebservice.user/login/" + username.getText() + "");
        req.setHttpMethod("GET");
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                byte[] data = (byte[]) req.getResponseData();
                String s = new String(data);
                User user = mapToJson(s);
                String pwd = StringUtil.replaceAll(user.getPassword(), "$2y", "$2a");
                if (BCrypt.checkpw(password.getText(), pwd) == true) {
                    System.out.println("SUCCESS " + password.getText());
                    User.setActifUser(user);
                    User.setIdOfConnectedUser(user.getId());
                    //redirect 
                    //main.updateLoginPhoto();
                    main.show();
                } else {
                    System.out.println("FAILED");
                    Dialog.show("Erreur", "Merci de vérifier vos paramétres de connexion", "Ok", null);
                }
            }
        });
        NetworkManager.getInstance().addToQueue(req);
    }

    private void showFacebookUser(String token) {
        ConnectionRequest req = new ConnectionRequest();
        req.setPost(false);
        req.setUrl("https://graph.facebook.com/v2.3/me");
        req.addArgumentNoEncoding("access_token", token);
        InfiniteProgress ip = new InfiniteProgress();
        Dialog d = ip.showInifiniteBlocking();
        NetworkManager.getInstance().addToQueueAndWait(req);
        byte[] data = req.getResponseData();
        JSONParser parser = new JSONParser();
        Map map = null;
        try {
            map = parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        String name = (String) map.get("name");
        d.dispose();
        Form userForm = new UserForm(name, (EncodedImage) theme.getImage("user.png"), "https://graph.facebook.com/v2.3/me/picture?access_token=" + token);
        userForm.show();
    }

    private void showGoogleUser(String token) {
        ConnectionRequest req = new ConnectionRequest();
        req.addRequestHeader("Authorization", "Bearer " + token);
        req.setUrl("https://www.googleapis.com/plus/v1/people/me");
        req.setPost(false);
        InfiniteProgress ip = new InfiniteProgress();
        Dialog d = ip.showInifiniteBlocking();
        NetworkManager.getInstance().addToQueueAndWait(req);
        d.dispose();
        byte[] data = req.getResponseData();
        JSONParser parser = new JSONParser();
        Map map = null;
        try {
            map = parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        String name = (String) map.get("displayName");
        Map im = (Map) map.get("image");
        String url = (String) im.get("url");
        Form userForm = new UserForm(name, (EncodedImage) theme.getImage("user.png"), url);
        userForm.show();
    }

    public class LoginListener extends LoginCallback {

        public static final int FACEBOOK = 0;

        public static final int GOOGLE = 1;

        private int loginType;

        public LoginListener(int loginType) {
            this.loginType = loginType;
        }

        public void loginSuccessful() {

            try {
                AccessToken token = login.getAccessToken();
                if (loginType == FACEBOOK) {
                    showFacebookUser(token.getToken());
                } else if (loginType == GOOGLE) {
                    showGoogleUser(token.getToken());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public void loginFailed(String errorMessage) {
            Dialog.show("Login Failed", errorMessage, "Ok", null);
        }
    }

    public User mapToJson(String json) {
        User user = new User();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> users = j.parseJSON(new CharArrayReader(json.toCharArray()));

            System.out.println(users);
            int userId = Integer.valueOf(users.get("id").toString().substring(0, users.get("id").toString().indexOf('.')));
            if (users != null) {
                user.setId(userId);
                user.setPassword(users.get("password").toString());
                user.setLogin(users.get("username").toString());
                try {
                    user.setDateNaissance(ToolsUtilities.formater.parse(users.get("datenaissance").toString()));
                } catch (ParseException ex) {

                }
                user.setEmail(users.get("email").toString());
                user.setEnabled(users.get("enabled").toString().equals("true") ? 1 : 0);
                user.setNom(users.get("nom").toString());
                user.setPrenom(users.get("prenom").toString());
                user.setPhotoProfil(users.get("profilePicture").toString());

            }
        } catch (IOException ex) {
        }
        return user;

    }

    public static boolean firstLogin() {
        return Storage.getInstance().readObject("token") == null;
    }

    public static void login(final Form form) {
        if (firstLogin()) {
            LoginForm logForm = new LoginForm(form);
            logForm.show();
        } else {
            //token exists no need to authenticate
            TOKEN = (String) Storage.getInstance().readObject("token");
            FaceBookAccess.setToken(TOKEN);
            //in case token has expired re-authenticate
            FaceBookAccess.getInstance().addResponseCodeListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    NetworkEvent ne = (NetworkEvent) evt;
                    int code = ne.getResponseCode();
                    //token has expired
                    if (code == 400) {
                        LoginForm logForm = new LoginForm(form);
                        logForm.show();
                    }
                }
            });
        }
    }

}
