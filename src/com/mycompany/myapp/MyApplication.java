package com.mycompany.myapp;

import Login.LoginForm;
import Utilities.ToolsUtilities;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.social.FacebookConnect;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.Enumeration;

public class MyApplication {

    private Form current;
    private static Resources theme;
    private Form main;

    private Command back = new Command("Back") {
        public void actionPerformed(ActionEvent evt) {
            killNetworkAccess();
            main.showBack();
        }
    };

    public void init(Object context) {
        try {
            theme = Resources.openLayered("/theme");
            UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        if (current != null) {
            current.show();
            return;
        }

        System.out.println("started");
        main = new Form("Acceuil");
        main.setScrollable(false);
        main.setLayout(new BorderLayout());

        final Command profileCommand = new Command("My Profile") {
            public void actionPerformed(ActionEvent evt) {
                updateLoginPhoto();
                main.getContentPane().removeAll();
                main.addComponent(BorderLayout.CENTER, showMyProfile());
                main.revalidate();
            }
        };

        main.addCommand(profileCommand);

        Command c = new Command("Categories");
        Label l = new Label("Categories") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        main.addCommand(c);

        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        main.addCommand(c1);

        main.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });
        main.addCommand(new Command("Logout") {
            public void actionPerformed(ActionEvent evt) {
                if (FacebookConnect.getInstance().isFacebookSDKSupported()) {
                    FacebookConnect.getInstance().logout();
                } else {
                    LoginForm login = new LoginForm(main);
                    login.show();
                }
            }
        });

        //redirect to login
        LoginForm login = new LoginForm(main);
        login.show();
        //main.show();
    }

    public void destroy() {
    }

    public void stop() {
    }

    private Component showMyProfile() {
        final Container c = new Container(new BorderLayout());
        BorderLayout bl = new BorderLayout();
        bl.setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE);
        Container p = new Container(bl);
        p.addComponent(BorderLayout.CENTER, new InfiniteProgress());

        c.addComponent(BorderLayout.CENTER, p);

        com.mycompany.Entite.User user = com.mycompany.Entite.User.getActifUser();

        int leftCol = Display.getInstance().getDisplayWidth() / 3;
        ComponentGroup gr = new ComponentGroup();
        gr.setLayout(new GridLayout(4, 1));
        gr.addComponent(getPairContainer("Name", user.getNom(), leftCol));
        gr.addComponent(getPairContainer("Prenom", user.getPrenom(), leftCol));
        gr.addComponent(getPairContainer("Email", user.getEmail(), leftCol));
        gr.addComponent(getPairContainer("Birthday", ToolsUtilities.formater.format(user.getDateNaissance()), leftCol));

        c.removeAll();
        c.addComponent(BorderLayout.CENTER, gr);

        Image i = getTheme().getImage("fbuser.jpg");
        Container imageCnt = new Container(new BorderLayout());
        Label myPic = new Label(i);
        imageCnt.addComponent(BorderLayout.NORTH, myPic);
        c.addComponent(BorderLayout.EAST, imageCnt);
        c.revalidate();

        return c;
    }

    private Container getPairContainer(String key, String val, int padding) {
        Label keyLabel = new Label(key);
        keyLabel.setUIID("Header");
        keyLabel.setPreferredW(padding);
        keyLabel.getStyle().setAlignment(Component.RIGHT);
        Label valLabel = new Label(val);
        valLabel.getStyle().setAlignment(Component.LEFT);
        Container cnt = new Container(new BoxLayout(BoxLayout.X_AXIS));
        cnt.addComponent(keyLabel);
        cnt.addComponent(valLabel);
        return cnt;
    }

    private void killNetworkAccess() {
        Enumeration e = NetworkManager.getInstance().enumurateQueue();
        while (e.hasMoreElements()) {
            ConnectionRequest r = (ConnectionRequest) e.nextElement();
            r.kill();
        }
    }

    public void updateLoginPhoto() {

        com.mycompany.Entite.User user = com.mycompany.Entite.User.getActifUser();
        //Image src = (Image) user.getPhotoProfil();
        System.out.println(user.getPhotoProfil());
        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(100, 100), true);
        Storage.getInstance().deleteStorageFile(user.getPhotoProfil());
        //Storage.getInstance().clearStorage();
        // Storage.getInstance().clearCache();
        Image src = URLImage.createToStorage(placeholder, "fileNameInStorage", user.getPhotoProfil(), URLImage.RESIZE_SCALE);

        if (src != null) {
            Command pc = new Command("", src) {
                public void actionPerformed(ActionEvent evt) {
                    main.getContentPane().removeAll();
                    main.addComponent(BorderLayout.CENTER, showMyProfile());
                    main.revalidate();
                }
            };
            main.addCommand(pc, main.getCommandCount());
        }
    }

    static Resources getTheme() {
        return theme;
    }

}
